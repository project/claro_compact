## SUMMARY

Claro Compact is a compact subtheme of Drupal's standard administration theme, Claro.

## DESCRIPTION

Claro, Drupal's new standard administration theme was designed with accessibility in mind.
However, the decision of forcing the drastically increased margins and spacing on everyone,
no matter whether they need it or not, is a questionable one.

Claro Compact is a subtheme that simply modifies the necessary items to retain full compatibility
with Claro, while making it more compact and, in our opinion, much easier to use. Nothing else
is changed but spacing.

## LINKS
Project page: https://www.drupal.org/project/claro_compact
